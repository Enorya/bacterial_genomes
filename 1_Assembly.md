This script allow the user to **assemble** a batch of **bacterial genomes**. It uses **fastqc**, **multiqc**, **trimmomatic**, **spades**, **medusa**, **gapcloser**, **prinseq-lite**, **barrnap**, **quast** and **clc_sequence_info**.

# Entries
**$1** = forward.fastq

**$2** = reverse.fastq

**$3** = basename (strains' IDs)

# 0.1 Create directories

    mkdir input/libconfig
    mkdir input/Ref_genomes
    mkdir 1_fastqc
    mkdir 2_trimmomatic
    mkdir 3_Spades
    mkdir 4_medusa
    mkdir 5_gapclose
    mkdir 6_finalAssembly
    mkdir 7_prokka
    mkdir 8_mage_submission
    mkdir 9_quast
    mkdir 10_clc

# 0.2 Make libary config files

    mkdir input/Ref_genomes/$3
    echo max_rd_len=155 > input/libconfig/$3_config.txt
    echo [LIB] >> input/libconfig/$3_config.txt
    echo avg_ins=816 >> input/libconfig/$3_config.txt
    echo reverse_seq=0 >> input/libconfig/$3_config.txt
    echo asm_flags=4 >> input/libconfig/$3_config.txt
    echo rd_len_cutoff=155 >> input/libconfig/$3_config.txt
    echo rank=1 >> input/libconfig/$3_config.txt
    echo pair_num_cutoff=3 >> input/libconfig/$3_config.txt
    echo map_len=32 >> input/libconfig/$3_config.txt
    echo q1=$1 >> input/libconfig/$3_config.txt
    echo q2=$2 >> input/libconfig/$3_config.txt

# 1.1 Quality control  with FastQC (get stats)

    mkdir 1_fastqc/$3
    fastqc -k 7 -o 1_fastqc/$3 $1 $2

# 1.2 Make Multiqc

    source multiqc_venv-0.9/bin/activate
    multiqc 1_fastqc/*_fastqc.zip

(possibility to change the second command like : 1_fastqc/_trimmed_fastqc.zip)
When it is finished :

    deactivate (unseat the virtual environment)

# 2.1 Trimming with Trimmomatic (cutting unwanted sequences)

    java -jar trimmomatic-0.38.jar PE -threads 10 -phred33 $1 $2 2_trimmomatic/$3_R1_paired.fastq.gz 2_trimmomatic/$3_R1_unpaired.fastq.gz 2_trimmomatic/$3_R2_paired.fastq.gz 2_trimmomatic/$3_R2_unpaired.fastq.gz ILLUMINACLIP:input/illumina.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36

# 2.2 Second round quality control with FastQC (to compare with raw reads)

    fastqc -k 7 -o 1_fastqc/$3 2_trimmomatic/$3_R1_paired.fastq.gz 2_trimmomatic/$3_R2_paired.fastq.gz

# 3.1 Assembly with SPAdes

    mkdir 3_Spades/$3
    SPAdes-3.12.0-Linux/bin/spades.py --pe1-1 2_trimmomatic/$3_R1_paired.fastq.gz  --pe1-2 2_trimmomatic/$3_R2_paired.fastq.gz  -o 3_Spades/$3  -t 4


# 3.2 Gather stats of the assemblies

    clc_sequence_info -c 1000 -n 3_Spades/$3/contigs.fasta >  3_Spades/$3/Genome_stats.txt

# 4. Find rDNA (used in blastn to find closest relative organism)

    barrnap --kingdom bac --threads 10 --outfasta 3_Spades/$3/rDNA.fasta 3_Spades/$3/contigs.fasta

# 5. Blastn vs bacteria (taxid:2)

Use **NCBI** web application of **blastn** to find closest relative organism and **download** its genome and set it in the directory *input/Ref_genomes/strain*

# 6. Use Medusa for scaffolding:

    medusa.sh -f input/Ref_genomes/$3/ -i 3_Spades/$3/contigs.fasta -o 4_medusa/$3.medusa.fasta –v 

# 7.1 Use GAPcloser to close MEDUSA scaffolds

    GapCloser -b input/libconfig/$3_config.txt -a 4_medusa/$3.medusa.fasta -l 155 -t 1 -o 5_gapclose/$3.medusa.gapclose.fasta


# 7.2 Gather stats of the scaffolding

    clc_sequence_info -c 1000 -n 5_gapclose/$3.medusa.gapclose.fasta > 5_gapclose/$3.medusa.gapclose.stats.txt

# 8. Filter sequences < 1kb with Prinseq-lite

    prinseq-lite.pl -min_len 1000 -out_good 6_finalAssembly/$3.medusa.gapclose.sup1kb -out_bad null -fasta 5_gapclose/$3.medusa.gapclose.fasta

# 9.1 Compare stats before and after assemblies with QUAST 

    mkdir 9_quast/$3
    quast.py 3_Spades/$3/contigs.fasta 5_gapclose/$3.medusa.gapclose.fasta -R input/Ref_genomes/$3/* -o 9_quast/$3/

# 9.2 Verify average coverage with clc_mapper

    clc_mapper -d 6_finalAssembly/$3.medusa.gapclose.sup1kb.fasta -o 10_clc/$3.cas -q -p fb ss 180 250 -i 2_trimmomatic/$3_R1_paired.fastq.gz 2_trimmomatic/$3_R2_paired.fastq.gz --cpus 12
    clc_mapping_info $3.cas
(possibility to add -c to have the average coverage of each contig)
