This script explains how to reconstruct metabolic networks of bacterial genomes from gbk files and how to compare them with the metabolic network of a host (here an alga). It also explains the creation of a gbk file with the IMG pipeline (you can find the scripts used in the 3rd step in the script branch).

# 1. Make sure the gbk files are fine

The gbk files need to contain a line **/db_xref="taxon:..."**, Otherwise the Pathway Tools command line is not going to work.
Also, the folder you give to the Pathway Tools command needs to contain **one folder for each gbk file**. The gbk **file and its folder** need to have the **same name** and this name needs to contain **at least 1 letter**.
If the command doesn't work when you launch all presented genomes at the same time, redo the command for less genomes (like 10 or 20).

## 1.1 Creation of gbk file for IMG

You are going to need the script on this link : https://github.com/ArnaudBelcour/gff_to_gbk/blob/img/gff_to_gbk.py
You need to download 3 files for each genome you annotate : **contigs.fna**, **functional_annotation.gff** and **proteins.faa**.
You also need to consider your **organism's name**.
Then, you can launch the command :

    python3 gbk_creator_from_gff.py -fg _contigs.fna -fp _proteins.faa -g _functional_annotation.gff -s organism's name -o your_output

# 2. Metabolic network reconstruction

First, you need to run **Pathway Tools (version 23.0)** on your genomes :

    mpwt -f genomes' folder -o /output_folder/newpgdb/ -v --patho --dat --md

Then, you need to **create a padmet file** from your newpgdb folder :

    for dir in /output_folder/newpgdb/* ; do python3 pgdb_to_padmet.py --output=/output_folder/padmet/$(basename $dir).padmet --directory=$dir -v -g --version=22.5 db=METACYC --padmetRef=metacyc_22.6.padmet ; done

Finally, you can **generate sbml files** from your new padmet files (you need to do this command for each genome):

    python3 sbmlGenerator.py --padmet=/output_folder/padmet/genome.padmet --output=/output_folder/sbml/genome.sbml --sbml_lvl=2 -v

# 3. Alignment of the metabolic networks

First, make sure that you installed **python 3.6**. Then you will be able to **install miscoto** :

    pip3 install pyasp (or pip3 install pyasp --no-cache-dir)
    pip3 install miscoto

After, the different miscoto command line can be run :

## 3.1 the command which allows you to extract new producible compounds 

    miscoto_scopes -m host.sbml -b output_folder/sbml/ -s seeds.xml -t targets.xml

Seeds = set of starting compounds

Targets = set of targeted compounds

## 3.2 the command which gives optimal solution

    miscoto_mincom -m host.sbml -b output_folder/sbml/ -s seeds.xml -t targets.xml -o soup

Here the tagets correspond to the compounds found with the miscoto_scopes command. 
you should see the selected bacteria in this step with their IDs/names

## 3.3 the command which Displays all the possible solution

    miscoto_mincom -m host.sbml -b output_folder/sbml/ -s seeds.xml -t targets.xml -o soup --enumeration

## 3.4 the command which indicates organisms shared by all the solutions

    miscoto_mincom -m host.sbml -b output_folder/sbml/ -s seeds.xml -t targets.xml -o soup --intersection

## 3.5 the command which points out a fusion of all the solutions (all the bacteria that participate to at least one solution)

    miscoto_mincom -m host.sbml -b output_folder/sbml/ -s seeds.xml -t targets.xml -o soup --union


For more details and information check out the link  https://github.com/cfrioux/miscoto
